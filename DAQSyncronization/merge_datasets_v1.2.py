"""
  Optogenetics and Neural Engineering Core ONE Core
  University of Colorado, School of Medicine
  27.Jan.2022
  See bit.ly/onecore for more information, including a more detailed write up.
  alignment_barcodes.py
################################################################################
  This code is designed to merge data from two DAQ datasets (after being aligned
  through timestamps/index values); in this particular case, data is being taken
  from processed LabJack .npy file, cleared to show only the data where barcodes
  were shared with NP, and then recursively searching through the OpenEphys NP
  recordings and "spike_times.npy" files to add this shared spike data as a new
  column or row within the LabJack data. This is then outputted as either .npy or .csv
  data.
################################################################################
  USER INPUTS EXPLAINED:

  Input Variables:
  = start_index = (int) The first index value when barcodes were synchronized
                  between LJ and NP data. This value should have been outputted
                  on the command line after running "alignment_barcodes.py"
  = final_index = (int) The last index value when barcodes were synchronized
                  between LJ and NP data. This value should have been outputted
                  on the command line after running "alignment_barcodes.py"

  Output Variables:
  = merger_name = (str) The name of the file(s) in which the output will be
                     saved in the chosen directory.
  = save_npy = (bool) Set to "True" to save the aligned data as a .npy file.
  = save_csv = (bool) Set to "True" to save the aligned data as a .csv file.

################################################################################
  References

"""
#######################
### Imports Section ###
#######################

import sys
import os
import numpy as np
from datetime import datetime
from pathlib import Path
from tkinter.filedialog import askdirectory, askopenfilename

################################################################################
############################ USER INPUT SECTION ################################
################################################################################

# Input variables
start_index = 4623794.0
final_index = 5822199.0

# Output Variables
merger_name = "LJ_NP_merged_data_final"
save_npy = True
save_csv = True

################################################################################
############################ END OF USER INPUT #################################
################################################################################

###################################
### Select Files for Data Merge ###
###################################

# Select the .npy file that contains the LJ data, and the folder in which the
# NP data is held and will be searched recursively.
try:
    LJ_dir_and_name = Path(askopenfilename(title = 'Select Numpy Processed LJ Data File'))
except:
    print("No Numpy LJ Data File Chosen")
    sys.exit()

try:
    LJ_numpy_data = np.load(LJ_dir_and_name)
except:
    LJ_numpy_data = ''
    print("LJ Numpy file not located/failed to load; please check the filepath")
    sys.exit()

try:
    NP_dir = Path(askdirectory(title = 'Select Main NeuroPixel Data Folder (contains the "experiments" directories)'))
except:
    print("No NeuroPixel Directory Selected")
    sys.exit()

# Collect all "spike_times.npy" filepaths within NP_dir, and save to a list for
# future recursive adding to data.
spike_files = []
sync_messages = []
for root,d_names,f_names in os.walk(NP_dir):
    for f in f_names:
        if f == 'spike_times.npy':
            spike_files.append(os.path.join(root, f))
        elif f == 'sync_messages.txt':
            with open(os.path.join(root, f)) as sync_file:
                sync_messages = sync_file.readlines()

print("Spike Files: ", spike_files)

# Have user select folder into which aligned data will be saved; if no format
# selected, inform the user.
if save_npy or save_csv:
    try:
        merger_dir = Path(askdirectory(title = 'Select Folder in which to Save Merged Data'))
    except:
        print("No Output Directory Selected")
        sys.exit()
else:
    print("Merged data will not be saved to file in any format.")

##########################################################
### Extract Recording Timestamp from sync_messages.txt ###
##########################################################

for line in sync_messages:
    if "subProcessor: 0" in line:  # Find subprocessor where recordings were done
        time_and_hz = line.split(' ')[-1] # Take the 'timestamp@30000Hz' segment
        recording_synctime = int(time_and_hz.split("@")[0])

###########################################
### Delete Extraneous Data from LJ Data ###
###########################################

# Locate the LJ index values whose timestamp column value matches the start_index
# or final_index values.
LJ_data_start = 0
data_start_set = False
LJ_data_end = 0

LJ_raw_num_rows = np.shape(LJ_numpy_data)[0]
for index in range(0,LJ_raw_num_rows):
    if start_index < LJ_numpy_data[index, 0] and data_start_set == False:
        LJ_data_start = index-1
        data_start_set = True
    elif final_index <= LJ_numpy_data[index, 0]:
        LJ_data_end = index
        break # Finish since both LJ_data_start and end are collected

# Narrow down the LJ_numpy_data based on LJ_data_start and LJ_data_end
saved_LJ_data = LJ_numpy_data[LJ_data_start:LJ_data_end+1]

# Attach a new column at end of saved_LJ_data to add spike_times.
spike_times_column = np.zeros((np.shape(saved_LJ_data)[0],1), dtype=saved_LJ_data.dtype)
merged_LJ_data = np.hstack((saved_LJ_data, spike_times_column))

########################################
### Add Spike Data to Merged LJ Data ###
########################################

# Run a recursive loop that loads the spike_times.npy arrays into a variable,
# pulls any index values that are between start_index and final_index, and then
# fits those into the merged_LJ_data where the index value is present.
new_spike_row = 0
spikes_del_index = []

for file in spike_files:    # Load spike_times.npy files recursively and select relevant data
    spike_data = np.load(file)
    synced_spike_data = spike_data + recording_synctime
    print("Raw Spike Data:", synced_spike_data)
    mergeable_spikes = synced_spike_data[np.where(
                          np.logical_and(synced_spike_data >= start_index,
                                         synced_spike_data <= final_index))]

    print("Spikes from this file which can be merged:", mergeable_spikes)
    for index in range(0, np.shape(merged_LJ_data)[0]):

        if mergeable_spikes == np.array([]): #If there are no more spikes to merge
            break #Stop the code and sort what has been handled

        LJ_timestamp = int(merged_LJ_data[index, 0]) # Testing just to see
        print("LJ Timestamp: ", LJ_timestamp)

        if index >= 1:
            top_index = merged_LJ_data[index-1, 0]
            bottom_index = merged_LJ_data[index, 0]
            for spike in range(0, np.shape(mergeable_spikes)[0]):
                spike_to_use = mergeable_spikes[spike]

                if top_index > spike_to_use: # Mainly used to remove any spikes prior to LJ merged datasets
                    spikes_del_index.append(spike) #add to list to be deleted

                elif top_index == spike_to_use: # Add spike to top_index
                    merged_LJ_data[index-1, -1] = 1
                    spikes_del_index.append(spike) #add to list to be deleted

                elif top_index < spike_to_use < bottom_index: # If spike is between current indices
                    new_spike_row = np.copy(merged_LJ_data[index-1]) # Copy top_index's data to new_spike_row
                    new_spike_row[0] = spike_to_use  # Set correct index value to new_spike_row
                    new_spike_row[-1] = 1 # Mark a spike on the end
                    merged_LJ_data = np.vstack((merged_LJ_data, new_spike_row)) #Add new row to merged_LJ_data for sorting
                    spikes_del_index.append(spike) #add spike to deletion list

                elif bottom_index == spike_to_use: # If spike matches bottom_index
                    merged_LJ_data[index, -1] = 1 # Mark spike for bottom_index
                    spikes_del_index.append(spike) #add to list to be deleted

                elif spike_to_use > bottom_index: # Spike is now outside of current scope
                    mergeable_spikes = np.delete(mergeable_spikes, spikes_del_index) # Delete the finished spikes from mergeable_spikes
                    spikes_del_index = []# Reset the spikes_del_index
                    break # end the mergeable_spike loop to move on to next merged index value

merged_LJ_data = merged_LJ_data[np.argsort(merged_LJ_data[:, 0])] #Arrange indices in correct order
################################################################
### Save Merged Data to Chosen File Format(s) ###
################################################################

# Test to see output here:
print("Final output for Merged LJ Data:\n", merged_LJ_data)

time_now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

if save_npy:
    output_file = merger_dir / (merger_name + time_now)
    np.save(output_file, merged_LJ_data)

if save_csv:
    output_file = merger_dir / (merger_name + time_now + '.csv')
    np.savetxt(output_file, merged_LJ_data, delimiter=',', fmt="%s")
